.. pylmt documentation master file, created by
   sphinx-quickstart on Wed Apr 15 10:35:47 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pylmt: Learning Modulo Theories in Python
=========================================

This the ``pylmt`` package, a Python implementation of (Structured)
Learning Modulo Theories [teso15]_.

LMT is a method for modeling and solving *hybrid* machine learning problems
containing:

* a mixture of Boolean and numerical variables, 

* a mixture of logical and linear arithmetical constraints.

In order to do so, it combines `Satisfiability Modulo Theories
<https://en.wikipedia.org/wiki/Satisfiability_modulo_theories>`_ -- or rather
Optimization Modulo Theories [sebastiani15a]_ [sebastiani15b]_ -- with
Structured Output Support Vector Machines [tsochantaridis05]_. For a thorough
description of the methods, please see the [teso15]_ paper.

If you make use of this software in a publication, we kindly ask you to cite
this paper [teso15]_.

Requirements
------------

``pylmt`` requires an Optimization Modulo Theory solver to run. There are only
two such solvers at the time of writing, `OptiMathSAT
<http://optimathsat.disi.unitn.it/>`_ and (a seemingly experimental branch of)
`z3 <http://z3.codeplex.com/>`_. At the moment ``pylmt`` only supports the
former.

``pylmt`` runs on top of (a very slightly modified version of) `pystruct
<https://pystruct.github.io/>`_ [mueller14]_; the modified version can be found
`here <https://github.com/stefanoteso/pystruct>`_.

Getting the components
----------------------

First of all, clone the repository::

    git clone https://bitbucket.org/stefanoteso/pylmt

to fetch a copy of ``pylmt``, and::

    git clone https://bitbucket.org/stefanoteso/pystruct

to fetch a copy of the modified ``pystruct``.

Running a ready-made example
----------------------------

To check that everything works, you can run the *Stairway to Heaven* example::

    cd experiments/sth
    PYTHONPATH="../..:$PYTHONPATH" ./run.py

For more information on the experiment, see the LMT paper [teso15]_.

Writing your own experiment
---------------------------

If you want to write your own learning problem, you will need to write down
an **input** file and one or more **template** files.

* The **input** file contains the training and test examples, one per line.
  Each line is composed of four semicolon-separated parts:

    #. The path to a **template** file encoding the OMT problem (see below.)

    #. A list of space-separated variable assignments to the input variables (the :math:`x` part), in LIBSMT2 format.

    #. A list of space-separated variable assignments to the output variables (the :math:`y` part), in LIBSMT2 format.

    #. A string, which can be either ``"train"`` or ``"test"``, indicating how the example should be used.

   See any of the ``experiments/sth/input-*`` files for a concrete example.

* Each **template** is a standard LIBSMT2 problem definition with some slight
  modifications:

    #. It must define *two* cost variables:

        #. A variable named ``cost``, which is minimized when performing inference and when computing the joint feature vector and the loss.

        #. A variable named ``separation``, which is minimized during training when performing loss-augmented inference.
    
    #. It must provide a number of special placeholder symbols, all starting
       with the ``%`` (percent) symbol.

       These placeholders are replaced by the ``LMTModel`` with actual values
       in order to produce a valid LIBSMT2 problem.

       The placeholders are used to inject input (``%x``), output (``%y``),
       weights (``%w_*``), the feature vector (``%DELTA_*``) and the cost
       function (``%cost``) into the template.

   See the generated ``experiments/sth/*.smt2`` files for inspiration.

The ``experiments/runner.py`` script provides an example of how to use the
``pylmt`` classes to read the files from disk and solve the learning problem.


License
-------

BSD, see the LICENSE file.

Contents
========

.. toctree::
   :maxdepth: 2

**LMT**

.. automodule:: pylmt.pylmt
    :members:

References
==========

.. [sebastiani15a] Roberto Sebastiani and Silvia Tomasi, **Optimization modulo theories with linear rational costs**, 2015.
.. [sebastiani15b] Roberto Sebastiani and Patrick Trentin, **OptiMathSAT: a tool for Optimization Modulo Theories**, 2015.
.. [teso15] Stefano Teso, Roberto Sebastiani and Andrea Passerini, **Structured Learning Modulo Theories**, 2014 (`PDF <http://arxiv.org/abs/1405.1675>`_).
.. [mueller14] Andreas Mueller and Sven Behnke, **PyStruct - Structured prediction in Python**, JMLR, 2014 (`PDF <http://jmlr.org/papers/v15/mueller14a.html>`_).
.. [tsochantaridis05] Ioannis Tsochantaridis *et al.*, **Large margin methods for structured and interdependent output variables**, 2005.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

