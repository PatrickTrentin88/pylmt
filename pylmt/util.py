# -*- coding: utf-8 -*-

import os
import subprocess
import fractions
import tempfile

def _cls(obj):
    return type(obj).__name__

def float2libsmt(x):
    z = fractions.Fraction(x)
    p, q = z.numerator, z.denominator
    if q == 1:
        ret = str(abs(p))
    elif p == q:
        ret = "1"
    else:
        ret = "(/ {} {})".format(abs(p), q)
    if p < 0:
        ret = "(- 0 {})".format(ret)
    return ret

def libsmt2float(s):
    parts = [part.strip("() ") for part in s.split()]
    parts = [part for part in parts if len(part)]
    assert parts[0] == "="
    if len(parts) == 3:
        if parts[2].startswith("true"):
            return 1.0
        elif parts[2].startswith("false"):
            return -1.0
        else:
            return float(parts[2])
    elif len(parts) == 5 and parts[2] == "/":
        # example: '(= FEAT_vertical_material (/ 1 2))'
        return float(parts[3]) / float(parts[4])
    elif len(parts) == 6 and parts[2] == "-" and parts[3] == "/":
        # example: '(= separation (- (/ 462499936509899335 81064793292668928)))'
        return -float(parts[4]) / float(parts[5])
    raise NotImplementedError("unhandled assignment string '{}': '{}'".format(s, parts))

class Binary(object):
    """A simple wrapper around binary executables."""
    def __init__(self, path):
        self.path = path

    def run(self, args, shell = True):
        pipe = subprocess.Popen(self.path + " " + " ".join(args),
                                stdout = subprocess.PIPE,
                                stderr = subprocess.PIPE,
                                shell = shell)
        out, err = pipe.communicate()
        ret = pipe.wait()
        return ret, out, err

class OptiMathSAT5(object):
    """A dumb wrapper around optimathsat5."""
    def __init__(self, path="optimathsat", debug=False):
        self._omt5 = Binary(path)
        self._debug = debug
        major, minor, release = self.get_version()
        version = major * 10000 + minor * 100 + release
        self.has_timeout_support = version >= 10307

    def _d(self, msg):
        if self._debug:
            print _cls(self), msg

    def _read_raw_assignments(self, lines):
        lines = [line for line in lines if not line.startswith("#")]
        if not lines[0] == "sat":
            raise RuntimeError("the problem is unsatisfiable!")
        assignments = {}
        for line in lines[1:]:
            parts = map(str.strip, line.split())
            parts = [part for part in parts if part not in ["(", ")"]]
            if not len(parts):
                continue
            parts[-1] = parts[-1][:-1]
            k, v = parts[0].lstrip("("), " ".join(parts[1:])
            assert sum(c == "(" for c in v) == sum(c == ")" for c in v), \
                   "unbalanced parens found in '{}'".format(v)
            assignments[k] = "(= {} {})".format(k, v)
        return assignments

    def optimize(self, problem, raw_model=True):
        """Optimize a LIBSMT2 problem instance.

        :param problem: a string representing the problem definition in LIBSMT2 format.
        :param raw_model: whether the returned model should be converted to \
            python values (default: ``False``).
        :returns: a dictionary mapping variable names to their values, the \
            error code returned by optimathsat, and the stdout and stderr \
            outputs.
        """
        fp = tempfile.NamedTemporaryFile(prefix="pylmt_", delete=False)
        try:
            fp.write(problem)
            fp.close()
            args = [fp.name]
            self._d("running '{}'".format(" ".join(args)))
            ret, out, err = self._omt5.run(args)
            assert ret == 0, "ERROR: OptiMathSat5: {}@{}:\n{}".format(cost_var, problem_path, err)
            self._d("done")
            if raw_model:
                assignments = self._read_raw_assignments(out.split("\n"))
            else:
                assignments = self._read_assignments(out.split("\n"))
        finally:
            if not self._debug:
                os.remove(fp.name)
        return assignments, ret, out, err

    def get_version(self):
        ret, out, err = self._omt5.run(["-version"])
        if ret != 0:
            raise RuntimeError("optimathsat does not appear to be installed")
        return map(int, out.split()[2].split("."))
