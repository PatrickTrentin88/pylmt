# -*- coding: utf-8 -*-
"""A Python implementation of (Structured) Learning Modulo Theories."""

import re
import numpy as np
import pystruct.models
import pystruct.learners
from pystruct.utils import SaveLogger
from collections import namedtuple
from pprint import pprint, pformat
from util import *

_LMTInput_ = namedtuple("LMTInput",
                        ("assignments", "variables", "features", "template"))
"""The input part of an object."""


class _LMTInput(_LMTInput_):
    def __repr__(self):
        return "LMTInput({})".format(pformat(self.assignments))

_LMTOutput_ = namedtuple("LMTOutput", ("assignments", "variables"))
"""The output part of an object."""


class _LMTOutput(_LMTOutput_):
    def __repr__(self):
        return "LMTOutput({})".format(pformat(self.assignments))


class LMTModel(pystruct.models.StructuredModel):
    """A max-margin LMT model.

    .. warning::

        The timeouts will only have effect if the underlying template file
        includes the ``(set-option :timeout %timeout )`` directive.

    :param features_to_index: map between feature names and feature index.
    :param omt_solver: OMT solver to use (default: ``"optimathsat5"``).
    :param fit_timeout: OMT optimization timeout for separation (default: ``None``).
    :param infer_timeout: OMT optimization timeout for inference (default: ``None``).
    :param debug: whether to spew debug output (default: ``False``).
    """
    def __init__(self, feature_to_index, omt_solver="optimathsat5",
                 fit_timeout=None, infer_timeout=None, debug=False):
        self._feature_to_index = feature_to_index
        if omt_solver == "optimathsat5":
            self._solver = OptiMathSAT5(debug=debug)
        else:
            raise ValueError("invalid omt_solver '{}'".format(omt_solver))
        self._fit_timeout = fit_timeout
        self._infer_timeout = infer_timeout
        self._debug = debug

        self.size_joint_feature = len(feature_to_index)
        self.inference_calls = 0

    def _d(self, message):
        if self._debug:
            print message

    def initialize(self, xs, ys):
        # no-op
        pass

    def init_latent(self):
        raise NotImplementedError

    def _fill_placeholders(self, line, **values):
        """Substitutes actual values to the placeholders in the template.

        :param line: string to perform the substitution on.
        :param x: input part of an object, as a list of assignments. It must \
            be provided.
        :param y: output part of an object, as a list of assignments. If \
            not provided, then the value of y is determined by the optimizer.
        :param w: current weights, as an ``numpy.ndarray``. If not provided, \
            then the value of the weights defaults to all ones.
        :param psi_x_y: feature representation of a training object, as an \
            ``numpy.ndarray``. If not provided, then the value defaults to \
            all zeros.
        :param cost: name of the cost variable.
        """
        filled_tokens = []
        for token in line.split():
            if token.startswith("%"):
                if token == "%x":
                    assert "x" in values, "x value is undefined"
                    token = "\n".join(values["x"])
                elif token == "%y":
                    if "y" in values:
                        token = "\n".join(values["y"])
                    else:
                        token = ""
                elif token.startswith("%w_FEAT"):
                    if not "w" in values:
                        token = "1"
                    else:
                        index = self._feature_to_index[token.split("_", 1)[-1]]
                        token = float2libsmt(values["w"][index])
                elif token.startswith("%DELTA"):
                    if not "psi_x_y" in values:
                        token = "0"
                    else:
                        index = self._feature_to_index[token.split("_", 1)[-1]]
                        # Here we flip the sign to turn the features into costs
                        token = float2libsmt(-values["psi_x_y"][index])
                elif token == "%cost":
                    assert "cost" in values, "cost variable is undefined"
                    token = values["cost"]
                elif token == "%timeout":
                    if not "timeout" in values or values["timeout"] is None:
                        # get rid of the whole `set-option :timeout` directive
                        filled_tokens = []
                        break
                    else:
                        token = "{}.0".format(values["timeout"])
                else:
                    raise RuntimeError("unrecognized token '{}'".format(token))
            filled_tokens.append(token)
        num_indent_spaces = 0
        for c in line:
            if not c == " ":
                break
            num_indent_spaces += 1
        filled_line = " " * num_indent_spaces + " ".join(filled_tokens)
        assert not '%' in filled_line, "failed to fill placeholder:\n {}".format(filled_line)
        return filled_line

    def _generate_omt_problem(self, template, **values):
        return "\n".join(map(lambda line: self._fill_placeholders(line, **values), template))

    def joint_feature(self, x, y):
        """Computes the joint feature representation :math:`\\psi(x,y)`.

        In order to do so we plug the values of :math:`x` and :math:`y` into
        the template and invoke the OMT solver, then read back the value of the
        features from the output.

        :param x: an input LMT object.
        :param y: an output LMT object.
        :returns: the joint feature representation as an ``np.ndarray``.
        """
        self._d("\n\n> Computing PSI of:\n{}\n{}".format(x, y))
        assert isinstance(x, _LMTInput), "x = {}:{}".format(x, type(x))
        assert isinstance(y, _LMTOutput), "y = {}:{}".format(y, type(y))
        zeros = [0.0] * self.size_joint_feature
        problem = self._generate_omt_problem(x.template,
                                             x=x.assignments,
                                             y=y.assignments,
                                             cost="cost")
        var_to_assignment, _, _, _ = self._solver.optimize(problem)
        psi, psi_name = zeros, [None] * self.size_joint_feature
        for feature, index in self._feature_to_index.iteritems():
            # Here we flip the sign to turn the costs into features
            psi[index] = -libsmt2float(var_to_assignment[feature])
            psi_name[index] = feature
        self._d("psi = {}".format(pformat(zip(psi_name, psi))))
        return np.array(psi)

    def inference(self, x, w, relaxed=None):
        """Computes the highest scoring output :math:`\\hat{y}`:

        .. math::
            \\hat{y} := \\text{argmax}_{y} w^\\top \\psi(x,y)

        where :math:`\\psi` is the joint feature function.
        """
        self._d("\n\n> Computing INFERENCE for:\n{}\nw={}".format(x, w))
        assert isinstance(x, _LMTInput)
        assert isinstance(w, np.ndarray)
        zeros = [0.0] * self.size_joint_feature
        problem = self._generate_omt_problem(x.template,
                                             x=x.assignments,
                                             w=w,
                                             cost="cost",
                                             timeout=self._infer_timeout)
        var_to_assignment, _, _, _ = self._solver.optimize(problem)
        # XXX we do not have access to the list of y variables here, so at the
        # moment we return *all* the assignments
        all_variables = sorted(var_to_assignment.keys())
        y_pred = _LMTOutput([var_to_assignment[var] for var in all_variables],
                            all_variables)
        self._d("y_pred = {}".format(y_pred))
        self.inference_calls += 1
        return y_pred

    def loss_augmented_inference(self, x, y, w, relaxed=None):
        """Finds the highest scoring wrong output :math:`\\hat{y}`:

        .. math::
            \\hat{y} :=
                \\text{argmax}_{y' \\neq y} w^\\top \\psi(x,y') + \
                    \\Delta(x,y,y')

        where :math:`\psi` is the joint feature function and :math:`\Delta` is
        the loss.

        First we compute :math:`\\psi(x,y)` to obtain the values of
        :math:`a_k(x,y)` and :math:`c_k(x,y)` used in :math:`\\Delta`; then we
        plug these values in the template and optimize the ``separation``
        objective function (rather than the ``cost`` objective function
        directly).

        :param x: the input part of an object.
        :param y: the output part of an object.
        :param w: the model weights.
        :returns: a highest scoring wrong LMT output.
        """
        psi_x_y = self.joint_feature(x, y)
        self._d("\n\n> Computing SEPARATION for:\n{}\n{}\npsi_x_y={}\nw={}".format(x, y, psi_x_y, w))
        assert isinstance(x, _LMTInput)
        assert isinstance(y, _LMTOutput)
        assert isinstance(w, np.ndarray)
        problem = self._generate_omt_problem(x.template,
                                             x=x.assignments,
                                             w=w,
                                             psi_x_y=psi_x_y,
                                             cost="separation",
                                             timeout=self._fit_timeout)
        var_to_assignment, _, _, _ = self._solver.optimize(problem)
        y_hat = _LMTOutput([var_to_assignment[var] for var in y.variables],
                           y.variables)
        self._d("y_hat = {}".format(y_hat))
        self.inference_calls += 1
        return y_hat

    def loss(self, x, y, y_hat):
        """Compute the sum of absolute differences between feature vectors.

        .. math::
            \\Delta(x,y,\\hat{y}) :=
                \\sum_{k:\\text{bool}} I(a_k(x,y) \\neq a_k(x,\\hat{y})) +
                \\sum_{k:\\text{real}} |c_k(x,y) - c_k(x,\\hat{y})|

        :param x: the input part of an object.
        :param y: the correct output part of the object.
        :param y_hat: the predicted output part of the object.
        :returns: the loss.
        """
        psi_x_y = self.joint_feature(x, y)
        psi_x_y_hat = self.joint_feature(x, y_hat)
        self._d("\n\n> Computing LOSS of:\n{}\n{}\n{}\n{}\n{}".format(x, y, psi_x_y, y_hat, psi_x_y_hat))
        assert isinstance(x, _LMTInput), "x = {}:{}".format(x, type(x))
        assert isinstance(y, _LMTOutput), "y = {}:{}".format(y, type(y))
        assert isinstance(y_hat, _LMTOutput), "y_hat = {}:{}".format(y_hat, type(y_hat))
        loss = np.sum(np.abs(psi_x_y - psi_x_y_hat))
        self._d("loss = {}".format(loss))
        return loss

    def continuous_loss(self, x, y, y_hat):
        return self.loss(x, y, y_hat)


class LMTRunner(object):
    """Main LMT class.

    It takes care of creating the actual ``LMTModel`` and the SSVM 1-slack
    CP solver, reading the dataset and templates.

    :param method: WRITEME
    :param dataset: WRITEME
    :param C: regularization hyperparameter.
    :param epsilon: precision hyperparameter.
    :param omt_solver: OMT solver to be used (default: ``"optimathsat5"``).
    :param fit_timeout: OMT solver timeout used during training, in seconds (default: ``None``).
    :param infer_timeout: OMT solver timeout used during inference, in seconds (default: ``None``).
    :param debug: whether to produce debug prints.
    """
    def __init__(self, method, dataset, C, epsilon, omt_solver="optimathsat5",
                 model_path="ssvm.pickle", fit_timeout=None, infer_timeout=None,
                 debug=False):
        self._train_xs, self._train_ys, self._test_xs, self._test_ys = \
            self._parse_dataset(dataset)

        if len(self._train_xs) != len(self._train_ys):
            raise ValueError("example inputs and outputs do not match")
        if not len(self._train_xs):
            raise ValueError("no examples to process")
        if len(self._test_xs) != len(self._test_ys):
            raise ValueError("example inputs and outputs do not match")
        if not len(self._test_xs):
            raise ValueError("no examples to process")
        self._debug = debug

        xs = self._train_xs + self._test_xs
        ys = self._train_ys + self._test_ys

        common_features = xs[0].features
        for x, y in zip(xs, ys):
            assert common_features == x.features, "features mismatch"
        feature_to_index = {feature: i for i, (feature, _)
                            in enumerate(common_features)}
        if debug:
            print "feature_to_index ="
            pprint(feature_to_index)

        self._index_to_feature = {v: k for k, v in feature_to_index.iteritems()}
        model = LMTModel(feature_to_index,
                         omt_solver=omt_solver,
                         fit_timeout=fit_timeout,
                         infer_timeout=infer_timeout,
                         debug=debug)

        logger = SaveLogger(model_path, save_every=1)

        ssvm = pystruct.learners.OneSlackSSVM(model,
                                              max_iter=int(1e10),
                                              C=C,
                                              tol=epsilon,
                                              check_constraints=debug,
                                              logger=logger,
                                              verbose=10)
        if method == "default":
            # no-op
            pass
        elif method == "latent":
            ssvm = pystruct.learners.LatentSSVM(base_ssvm=ssvm,
                                                latent_iter=10)
        else:
            raise ValueError("invalid method '{}'".format(method))
        self._ssvm = ssvm

    def run(self):
        # TODO try to guess the best value for the latent variables, see
        # https://pystruct.github.io/generated/pystruct.learners.LatentSSVM.html#pystruct.learners.LatentSSVM
        self._ssvm.fit(self._train_xs, self._train_ys)
        if self._debug:
            print "\n\n> training done, model WEIGHTS ="
            for i, feature in sorted(self._index_to_feature.iteritems()):
                print i, feature, self._ssvm.w[i]
        return self._ssvm.predict(self._test_xs)

    def _parse_assignments(self, string):
        assignments = map(str.strip, string.split(","))
        variables = [assignment.strip("() ").split()[1]
                     for assignment in assignments]
        return variables, assignments

    def _parse_dataset(self, path):
        """Parses an input file.

        The input file holds one example per line.  A line should have three
        semicolon separated parts, namely:

        #. the path to the LIBSMT2 template for the example.
        #. the x part of the object (the input assignments).
        #. the y part of the object (the output assignments).
        #. either ``"train"`` or ``"test"`` to indicate how to use example.

        :param path: path to the input file.
        :returns: a pair of lists of inputs and outputs taken from the input
            file.
        """
        train_xs, train_ys, test_xs, test_ys = [], [], [], []
        with open(path, "rb") as fp:
            for ln, line in enumerate(map(str.strip, fp)):
                parts = map(str.strip, line.split(";"))
                if len(parts) != 4:
                    raise SyntaxError("invalid dataset: {}: '{}'".format(ln, line))
                template_path, x_str, y_str, mode = parts[0], parts[1], parts[2], parts[3]
                if not mode in ("train", "test"):
                    raise SyntaxError("invalid example mode: {}: '{}'".format(ln, line))
                template, features = self._parse_template(template_path)
                x_variables, x_assignments = self._parse_assignments(x_str)
                y_variables, y_assignments = self._parse_assignments(y_str)
                x = _LMTInput(x_assignments, x_variables, features, template)
                y = _LMTOutput(y_assignments, y_variables)
                if mode == "train":
                    train_xs.append(x)
                    train_ys.append(y)
                else:
                    test_xs.append(x)
                    test_ys.append(y)
        return train_xs, train_ys, test_xs, test_ys

    def _parse_template(self, path):
        """Parses a template file."""
        expr = re.compile(r"^\(declare-fun FEAT_([a-zA-Z0-9_]*) \(\) ([a-zA-Z]+)\)$")
        template, features = [], []
        with open(path, "rb") as fp:
            for line in fp:
                template.append(line)
                match = expr.match(line)
                if match:
                    feature = "FEAT_" + match.groups()[0]
                    feature_type = match.groups()[1]
                    features.append((feature, feature_type))
        assert len(features) == len(set(features))
        return template, features
