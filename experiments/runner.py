#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from pylmt import LMTRunner

def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("method", type = str,
                        help = "method to use, either 'default' or 'latent'")
    parser.add_argument("input", type = str,
                        help = "path to the input")
    parser.add_argument("--debug", action = "store_true",
                        help = "enable debug spew")
    group = parser.add_argument_group("SO-SVM solver options")
    group.add_argument("-C", type = float, default = 1.0,
                       help="error penalty (default: 1.0)")
    group.add_argument("--epsilon", type = float, default = 1e-6,
                       help = "termination condition (default: 1e-6)")
    group = parser.add_argument_group("OMT solver options")
    group.add_argument("--omt-solver", type = str, default = "optimathsat5",
                       help = "OMT solver to use (default: optimathsat5)")
    group.add_argument("--fit-timeout", type = int, default = 0,
                       help = "OMT solver timeout, used when fitting the model (default: no timeout)")
    group.add_argument("--infer-timeout", type = int, default = 0,
                       help = "OMT solver timeout, used when making predictions (default: no timeout)")
    args = parser.parse_args()

    lmt = LMTRunner(args.method, args.input,
                    args.C, args.epsilon,
                    omt_solver = args.omt_solver,
                    fit_timeout = args.fit_timeout,
                    infer_timeout = args.infer_timeout,
                    debug = args.debug)

    xs, ys, pred_ys = lmt.run()

    for i, (x, y, pred_y) in enumerate(zip(xs, ys, pred_ys)):
        print "==== PREDICTIONS FOR EXAMPLE {}/{} ====".format(i, len(xs))
        print "x =", x.assignments
        print "y =", y.assignments
        print "pred_y =", [pred_y.assignments[var] for var in y.variables]
        print

if __name__ == "__main__":
    main()
