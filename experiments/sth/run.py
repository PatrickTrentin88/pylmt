#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import sys
import os
import re
import numpy as np
import Image, ImageDraw, ImageOps
from collections import defaultdict
from pylmt import LMTRunner
from pylmt import libsmt2float

C = 1000000
EPSILON = 1e-15
INFTY = 10000
PNG_SIZE = 512


def generate_template(n, dmin=0, dmax=1):
    """Generates a template LIBSMT2 file to be used with LMT.

    The template file is a standard LIBSMT2 problem with special tokens and
    variables used to plug in the inputs/outputs/weights and read off the
    predictions and loss.

    :param n: number of blocks in the stairway.
    :param dmin: lower bound on all coordinates (default: 0).
    :param max: upper bound on all coordinates (default: 1).
    """
    template = []

    # Debug

    blocks = range(1, n + 1)
    template.append(";; blocks = {}".format(blocks))

    template.append("(set-logic QF_LRA)")
    template.append("(set-option :produce-models true)")
    template.append("(set-option :timeout %timeout )")

    # Variables

    template.append("\n;; variable: cost")
    template.append("(declare-fun cost () Real)")

    template.append("\n;; variable: delta")
    template.append("(declare-fun delta () Real)")

    template.append("\n;; variable: separation")
    template.append("(declare-fun separation () Real)")

    template.append("\n;; variable: block coordinates")
    for i in blocks:
        template.append("(declare-fun x_%d () Real)" %i)
        template.append("(declare-fun y_%d () Real)" %i)

    template.append("\n;; variable: block dimensions")
    for i in blocks:
        template.append("(declare-fun dx_%d () Real)" %i)
        template.append("(declare-fun dy_%d () Real)" %i)

    template.append("\n;; auxhiliary variable: left_i_i+1")
    for i in range (1,n):
        template.append("(declare-fun left_%d_%d () Bool)" % (i,i+1))

    template.append("\n;; auxhiliary variable: below_i_i+1")
    for i in range (1,n):
        template.append("(declare-fun below_%d_%d () Bool)" % (i,i+1))

    template.append("\n;; auxhiliary variable: over_i_i+1")
    for i in range (1,n):
        template.append("(declare-fun over_%d_%d () Bool)" % (i,i+1))

    template.append("\n;; auxhiliary variable: bottom_left_i")
    for i in blocks:
        template.append("(declare-fun bottom_left_%d () Bool)" %i)

    template.append("\n;; auxhiliary variable: bottom_right_i")
    for i in blocks:
        template.append("(declare-fun bottom_right_%d () Bool)" %i)

    template.append("\n;; auxhiliary variable: top_left_i")
    for i in blocks:
        template.append("(declare-fun top_left_%d () Bool)" %i)

    template.append("\n;; auxhiliary variable: top_right_i")
    for i in blocks:
        template.append("(declare-fun top_right_%d () Bool)" %i)

    template.append("\n;; auxhiliary variable: left_step_i_i+1")
    for i in range (1,n):
        template.append("(declare-fun left_step_%d_%d () Bool)" % (i,i+1))

    template.append("\n;; auxhiliary variable: right_step_i_i+1")
    for i in range (1,n):
        template.append("(declare-fun right_step_%d_%d () Bool)" % (i,i+1))

    template.append("\n;; auxhiliary variable: c_step_height_left_i_i+1")
    for i in range(1,n):
        template.append("(declare-fun c_step_height_left_%d_%d () Real)" % (i,i+1))

    template.append("\n;; auxhiliary variable: c_step_height_left_n_n+1")
    template.append("(declare-fun c_step_height_left_%d_%d () Real)" % (n,n+1))

    template.append("\n;; auxhiliary variable: c_step_height_right_0_1")
    template.append("(declare-fun c_step_height_right_0_1 () Real)")

    template.append("\n;; auxhiliary variable: c_step_height_right_i_i+1")
    for i in range(1,n):
        template.append("(declare-fun c_step_height_right_%d_%d () Real)" % (i,i+1))

    template.append("\n;; auxhiliary variable: c_step_width_left_0_1")
    template.append("(declare-fun c_step_width_left_0_1 () Real)")

    template.append("\n;; auxhiliary variable: c_step_width_left_i_i+1")
    for i in range(1,n):
        template.append("(declare-fun c_step_width_left_%d_%d () Real)" % (i,i+1))

    template.append("\n;; auxhiliary variable: c_step_width_right_i_i+1")
    for i in range(1,n):
        template.append("(declare-fun c_step_width_right_%d_%d () Real)" % (i,i+1))

    template.append("\n;; auxhiliary variable: c_step_width_right_n_n+1")
    template.append("(declare-fun c_step_width_right_%d_%d () Real)" % (n,n+1))

    template.append("\n;; auxhiliary variable: left_ladder")
    template.append("(declare-fun left_ladder () Bool)")

    template.append("\n;; auxhiliary variable: right_ladder")
    template.append("(declare-fun right_ladder () Bool)")

    feats = (
        "max_step_height_left",  "min_step_height_left",
        "max_step_width_left", "min_step_width_left",
        "max_step_height_right",  "min_step_height_right",
        "max_step_width_right", "min_step_width_right",
        "vertical_material", "horizontal_material",
    )

    template.append("\n;; auxiliary variable: features (costs)")
    for feat in feats:
        template.append("(declare-fun FEAT_%s () Real)" % feat)

    template.append("\n;; auxiliary variable: per-feature delta component")
    for feat in feats:
        template.append("(declare-fun DELTA_%s () Real)" % feat)

    # Problem definition

    template.append("\n;; problem")
    template.append("(assert")
    template.append("   (and")

    template.append("\n       ;; constraint: hack")
    template.append("       (< cost %d)" % INFTY)

    # block coordinate constraints

    template.append("\n       ;; constraint: coordinates")
    for i in blocks:
        template.append("       (and (>= x_%d 0) (<= x_%d %d))" % (i,i,dmax))
        template.append("       (and (>= y_%d 0) (<= y_%d %d))" % (i,i,dmax))
        template.append("       (and (>= dx_%d %d) (<= dx_%d %d))" % (i,dmin,i,dmax))
        template.append("       (and (>= dy_%d %d) (<= dy_%d %d))" % (i,dmin,i,dmax))

    # inside image constraints

    template.append("\n       ;; constraint: inside image")
    for i in blocks:
        template.append("       (<= (+ x_%d dx_%d) %d)" % (i,i,dmax))
        template.append("       (<= (+ y_%d dy_%d) %d)" % (i,i,dmax))

    # no overlap constraints

    template.append("\n       ;; constraint: no overlap")
    for i in blocks:
        for j in range(i+1,n+1):
            template.append("       (or (<= (+ x_%d dx_%d) x_%d)" % (i,i,j))
            template.append("           (<= (+ y_%d dy_%d) y_%d)" % (i,i,j))
            template.append("           (<= (+ x_%d dx_%d) x_%d)" % (j,j,i))
            template.append("           (<= (+ y_%d dy_%d) y_%d)" % (j,j,i))
            template.append("       )")

    # left constraints

    template.append("\n       ;; constraint: left_i_i+1")
    for i in range(1,n):
        template.append("       (=> left_%d_%d" % (i,i+1))
        template.append("            (and (= (+ x_%d dx_%d) x_%d)" % (i,i,i+1))
        template.append("                 (or (and (<= y_%d y_%d) (<= y_%d (+ y_%d dy_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= y_%d (+ y_%d dy_%d)) (<= (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("       )")
    for i in range(1,n):
        template.append("       (=> ")
        template.append("            (and (= (+ x_%d dx_%d) x_%d)" % (i,i,i+1))
        template.append("                 (or (and (<= y_%d y_%d) (<= y_%d (+ y_%d dy_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= y_%d (+ y_%d dy_%d)) (<= (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("       left_%d_%d)" % (i,i+1))

    template.append("\n       ;; constraint: below_i_i+1")
    for i in range(1,n):
        template.append("       (=> below_%d_%d" % (i,i+1))
        template.append("            (and (= (+ y_%d dy_%d) y_%d)" % (i,i,i+1))
        template.append("                 (< y_%d y_%d)" % (i,i+1))
        template.append("                 (or (and (<= x_%d x_%d) (<= x_%d (+ x_%d dx_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= x_%d (+ x_%d dx_%d)) (<= (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("        )")
    for i in range(1,n):
        template.append("       (=>")
        template.append("            (and (= (+ y_%d dy_%d) y_%d)" % (i,i,i+1))
        template.append("                 (< y_%d y_%d)" % (i,i+1))
        template.append("                 (or (and (<= x_%d x_%d) (<= x_%d (+ x_%d dx_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= x_%d (+ x_%d dx_%d)) (<= (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("        below_%d_%d)" % (i,i+1))

    template.append("\n       ;; constraint: over_i_i+1")
    for i in range(1,n):
        template.append("       (=> over_%d_%d" % (i,i+1))
        template.append("            (and (= (+ y_%d dy_%d) y_%d)" % (i+1,i+1,i))
        template.append("                 (> y_%d y_%d)" % (i,i+1))
        template.append("                 (or (and (<= x_%d x_%d) (<= x_%d (+ x_%d dx_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= x_%d (+ x_%d dx_%d)) (<= (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("       )")
    for i in range(1,n):
        template.append("       (=>")
        template.append("            (and (= (+ y_%d dy_%d) y_%d)" % (i+1,i+1,i))
        template.append("                 (> y_%d y_%d)" % (i,i+1))
        template.append("                 (or (and (<= x_%d x_%d) (<= x_%d (+ x_%d dx_%d)))" % (i+1,i,i,i+1,i+1))
        template.append("                     (and (<= x_%d (+ x_%d dx_%d)) (<= (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i+1,i,i,i,i,i+1,i+1))
        template.append("                 )")
        template.append("            )")
        template.append("       over_%d_%d)" % (i,i+1))

    template.append("\n       ;; constraint: bottom_left_i")
    for i in blocks:
        template.append("       (=> bottom_left_%d" % i)
        template.append("            (and (= x_%d 0) (= y_%d 0))" % (i,i))
        template.append("        )")
    for i in blocks:
        template.append("       (=>")
        template.append("            (and (= x_%d 0) (= y_%d 0))" % (i,i))
        template.append("        bottom_left_%d)" % (i))

    template.append("\n       ;; constraint: bottom_right_i")
    for i in blocks:
        template.append("       (=> bottom_right_%d" % i)
        template.append("            (and (= (+ x_%d dx_%d) %d) (= y_%d 0))" % (i,i,dmax,i))
        template.append("        )")
    for i in blocks:
        template.append("       (=>")
        template.append("            (and (= (+ x_%d dx_%d) %d) (= y_%d 0))" % (i,i,dmax,i))
        template.append("        bottom_right_%d)" % (i))

    template.append("\n       ;; constraint: top_left_i")
    for i in blocks:
        template.append("       (=> top_left_%d" % i)
        template.append("            (and (= x_%d 0) (= (+ y_%d dy_%d) %d))" % (i,i,i,dmax))
        template.append("        )")
    for i in blocks:
        template.append("       (=>")
        template.append("            (and (= x_%d 0) (= (+ y_%d dy_%d) %d))" % (i,i,i,dmax))
        template.append("        top_left_%d)" % (i))

    template.append("\n       ;; constraint: top_right_i")
    for i in blocks:
        template.append("       (=> top_right_%d" % i)
        template.append("            (and (= (+ x_%d dx_%d) %d) (= (+ y_%d dy_%d) %d))" % (i,i,dmax,i,i,dmax))
        template.append("        )")
    for i in blocks:
        template.append("       (=>")
        template.append("            (and (= (+ x_%d dx_%d) %d) (= (+ y_%d dy_%d) %d))" % (i,i,dmax,i,i,dmax))
        template.append("        top_right_%d)" % (i))

    # step constraints

    template.append("\n       ;; constraint: left_step_i_i+1")
    for i in range(1,n):
        template.append("       (=> left_step_%d_%d" % (i,i+1))
        template.append("           (or (and left_%d_%d (> (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("               (and over_%d_%d (< (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("           )")
        template.append("       )")
    for i in range(1,n):
        template.append("       (=>")
        template.append("           (or (and left_%d_%d (> (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("               (and over_%d_%d (< (+ x_%d dx_%d) (+ x_%d dx_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("           )")
        template.append("       left_step_%d_%d)" % (i,i+1))

    template.append("\n       ;; constraint: right_step_i_i+1")
    for i in range(1,n):
        template.append("       (=>")
        template.append("           (or (and left_%d_%d (< (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("               (and below_%d_%d (< x_%d x_%d))" % (i,i+1,i,i+1))
        template.append("           )")
        template.append("       right_step_%d_%d)" % (i,i+1))
    for i in range(1,n):
        template.append("       (=> right_step_%d_%d" % (i,i+1))
        template.append("           (or (and left_%d_%d (< (+ y_%d dy_%d) (+ y_%d dy_%d)))" % (i,i+1,i,i,i+1,i+1))
        template.append("               (and below_%d_%d (< x_%d x_%d))" % (i,i+1,i,i+1))
        template.append("           )")
        template.append("       )")

    # costs

    template.append("\n       ;; constraint: step_height_left_i_i+1")
    for i in range(1,n):
        template.append("       (=> left_step_%d_%d (= c_step_height_left_%d_%d (+ y_%d (- dy_%d (+ y_%d dy_%d)))))" % (i,i+1,i,i+1,i,i,i+1,i+1))
        template.append("       (=> (not left_step_%d_%d) (= c_step_height_left_%d_%d %d))" % (i,i+1,i,i+1,dmax))

    template.append("\n       ;; constraint: step_height_left_n_n+1")
    template.append("       (=> bottom_right_%d (= c_step_height_left_%d_%d (- dy_%d y_%d)))" % (n,n,n+1,n,n))
    template.append("       (=> (not bottom_right_%d) (= c_step_height_left_%d_%d %d))" % (n,n,n+1,dmax))

    template.append("\n       ;; constraint: step_height_right_0_1")
    template.append("       (=> bottom_left_1 (= c_step_height_right_0_1 (- dy_1 y_1)))")
    template.append("       (=> (not bottom_left_1) (= c_step_height_right_0_1 %d))"  % dmax)

    template.append("\n       ;; constraint: step_height_right_i_i+1")
    for i in range(1,n):
        template.append("       (=> right_step_%d_%d (= c_step_height_right_%d_%d (+ y_%d (- dy_%d (+ y_%d dy_%d)))))" % (i,i+1,i,i+1,i+1,i+1,i,i))
        template.append("       (=> (not right_step_%d_%d) (= c_step_height_right_%d_%d %d))" % (i,i+1,i,i+1,dmax))




    template.append("\n       ;; constraint: step_width_left_0_1")
    template.append("       (=> top_left_1 (= c_step_width_left_0_1 dx_1))")
    template.append("       (=> (not top_left_1) (= c_step_width_left_0_1 0))")

    template.append("\n       ;; constraint: step_width_left_i_i+1")
    for i in range(1,n):
        template.append("       (=> left_step_%d_%d (= c_step_width_left_%d_%d (+ x_%d (- dx_%d (+ x_%d dx_%d)))))" % (i,i+1,i,i+1,i+1,i+1,i,i))
        template.append("       (=> (not left_step_%d_%d) (= c_step_width_left_%d_%d 0))" % (i,i+1,i,i+1))

    template.append("\n       ;; constraint: step_width_right_i_i+1")
    for i in range(1,n):
        template.append("       (=> right_step_%d_%d (= c_step_width_right_%d_%d (- x_%d x_%d)))" % (i,i+1,i,i+1,i+1,i))
        template.append("       (=> (not right_step_%d_%d) (= c_step_width_right_%d_%d 0))" % (i,i+1,i,i+1))

    template.append("\n       ;; constraint: step_width_right_%d_%d" % (n,n+1))
    template.append("       (=> top_right_%d (= c_step_width_right_%d_%d dx_%d))" % (n,n,n+1,n))
    template.append("       (=> (not top_right_%d) (= c_step_width_right_%d_%d 0))" % (n,n,n+1))

    # objectives

#    template.append("\n       ;; there should be a ladder")
#    template.append("       (or left_ladder right_ladder)")

    template.append("\n       ;; left_ladder")
    template.append("       (=> left_ladder (and top_left_1")
    for i in range(1,n):
        template.append("                            left_step_%d_%d" % (i,i+1))
    template.append("                            bottom_right_%d" % (n))
    template.append("                       )")
    template.append("       )")
    template.append("       (=> (and top_left_1")
    for i in range(1,n):
        template.append("                            left_step_%d_%d" % (i,i+1))
    template.append("                            bottom_right_%d" % (n))
    template.append("                       )")
    template.append("       left_ladder)")

    template.append("\n       ;; right_ladder")
    template.append("       (=> right_ladder (and bottom_left_1")
    for i in range(1,n):
        template.append("                             right_step_%d_%d" % (i,i+1))
    template.append("                             top_right_%d" % (n))
    template.append("                       )")
    template.append("       )")
    template.append("       (=> (and bottom_left_1")
    for i in range(1,n):
        template.append("                             right_step_%d_%d" % (i,i+1))
    template.append("                             top_right_%d" % (n))
    template.append("                       )")
    template.append("       right_ladder)")

    template.append("\n       ;; left_ladder height cost (max step height)")
    for i in range(1,n+1):
        template.append("       (<= (* %d c_step_height_left_%d_%d) FEAT_max_step_height_left)" % (n,i,i+1))
    template.append("(=> left_ladder (or")
    for i in range(1,n+1):
        template.append("(= (* %d c_step_height_left_%d_%d) FEAT_max_step_height_left)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not left_ladder) (= FEAT_max_step_height_left %d))" % (dmax*n))

    template.append("\n       ;; left_ladder height cost (min step height)")
    for i in range(1,n+1):
        template.append("       (>= (* %d c_step_height_left_%d_%d) FEAT_min_step_height_left)" % (n,i,i+1))
    template.append("(=> left_ladder (or")
    for i in range(1,n+1):
        template.append("(= (* %d c_step_height_left_%d_%d) FEAT_min_step_height_left)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not left_ladder) (= FEAT_min_step_height_left %d))" % (0))



    template.append("\n       ;; right_ladder height cost (max step height)")
    for i in range(0,n):
        template.append("       (<= (* %d c_step_height_right_%d_%d) FEAT_max_step_height_right)" % (n, i,i+1))
    template.append("(=> right_ladder (or")
    for i in range(0,n):
        template.append("(= (* %d c_step_height_right_%d_%d) FEAT_max_step_height_right)" % (n, i,i+1))
    template.append("))")
    template.append("(=> (not right_ladder) (= FEAT_max_step_height_right %d))" % (dmax*n))

    template.append("\n       ;; right_ladder height cost (min step height)")
    for i in range(0,n):
        template.append("       (>= (* %d c_step_height_right_%d_%d) FEAT_min_step_height_right)" % (n, i,i+1))
    template.append("(=> right_ladder (or")
    for i in range(0,n):
        template.append("(= (* %d c_step_height_right_%d_%d) FEAT_min_step_height_right)" % (n, i,i+1))
    template.append("))")
    template.append("(=> (not right_ladder) (= FEAT_min_step_height_right %d))" % (0))



    template.append("\n       ;; left ladder width cost (max step width)")
    for i in range(0,n):
        template.append("       (<= (* %d c_step_width_left_%d_%d) FEAT_max_step_width_left)" % (n,i,i+1))
    template.append("(=> left_ladder (or")
    for i in range(0,n):
        template.append("(= (* %d c_step_width_left_%d_%d) FEAT_max_step_width_left)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not left_ladder) (= FEAT_max_step_width_left %d))" % (dmax*n))

    template.append("\n       ;; left ladder width cost (min step width)")
    for i in range(0,n):
        template.append("       (>= (* %d c_step_width_left_%d_%d) FEAT_min_step_width_left)" % (n,i,i+1))
    template.append("(=> left_ladder (or")
    for i in range(0,n):
        template.append("(= (* %d c_step_width_left_%d_%d) FEAT_min_step_width_left)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not left_ladder) (= FEAT_min_step_width_left %d))" % (0))



    template.append("\n       ;; right ladder max width")
    for i in range(1,n+1):
        template.append("       (<= (* %d c_step_width_right_%d_%d) FEAT_max_step_width_right)" % (n,i,i+1))
    template.append("(=> right_ladder (or")
    for i in range(1,n+1):
        template.append("(= (* %d c_step_width_right_%d_%d) FEAT_max_step_width_right)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not right_ladder) (= FEAT_max_step_width_right %d))" % (dmax*n))

    template.append("\n       ;; right ladder width cost (min step width)")
    for i in range(1,n+1):
        template.append("       (>= (* %d c_step_width_right_%d_%d) FEAT_min_step_width_right)" % (n,i,i+1))
    template.append("(=> right_ladder (or")
    for i in range(1,n+1):
        template.append("(= (* %d c_step_width_right_%d_%d) FEAT_min_step_width_right)" % (n,i,i+1))
    template.append("))")
    template.append("(=> (not right_ladder) (= FEAT_min_step_width_right %d))" % (0))



    template.append("\n       ;; vertical_material")
    template.append("       (= FEAT_vertical_material (/ (+")
    for i in blocks:
        template.append("                                dy_%d" %i)
    template.append("                              ) %d)" % n)
    template.append("       )")

    template.append("\n       ;; horizontal_material")
    template.append("       (= FEAT_horizontal_material (/ (+")
    for i in blocks:
        template.append("                                dx_%d" %i)
    template.append("                              ) %d)" % n)
    template.append("       )")

    # Compute cost

#    template.append("\n       ;; cost function")
#    template.append("       (=> left_ladder")
#    template.append("                (= cost (+")
#    template.append("                        (* %w_FEAT_max_step_height_left    FEAT_max_step_height_left)")
#    template.append("                        (* %w_FEAT_min_step_width_left     FEAT_min_step_width_left)")
#    template.append("                        (* %w_FEAT_vertical_material   FEAT_vertical_material)")
#    template.append("                        (* %w_FEAT_horizontal_material FEAT_horizontal_material)))")
#    template.append("       )")
#    template.append("       (=> right_ladder")
#    template.append("                (= cost (+")
#    template.append("                        (* %w_FEAT_max_step_height_right   FEAT_max_step_height_right)")
#    template.append("                        (* %w_FEAT_min_step_width_right    FEAT_min_step_width_right)")
#    template.append("                        (* %w_FEAT_vertical_material   FEAT_vertical_material)")
#    template.append("                        (* %w_FEAT_horizontal_material FEAT_horizontal_material)))")
#    template.append("       )")
    template.append("\n       ;; cost function")
    template.append("        (= cost (+")
    template.append("                (* %w_FEAT_max_step_height_left    FEAT_max_step_height_left)")
    template.append("                (* %w_FEAT_min_step_height_left    FEAT_min_step_height_left)")
    template.append("                (* %w_FEAT_max_step_width_left     FEAT_max_step_width_left)")
    template.append("                (* %w_FEAT_min_step_width_left     FEAT_min_step_width_left)")
    template.append("                (* %w_FEAT_max_step_height_right   FEAT_max_step_height_right)")
    template.append("                (* %w_FEAT_min_step_height_right   FEAT_min_step_height_right)")
    template.append("                (* %w_FEAT_max_step_width_right    FEAT_max_step_width_right)")
    template.append("                (* %w_FEAT_min_step_width_right    FEAT_min_step_width_right)")
    template.append("                (* %w_FEAT_vertical_material   FEAT_vertical_material)")
    template.append("                (* %w_FEAT_horizontal_material FEAT_horizontal_material)))")

    # Compute loss

    template.append("\n       ;; delta components")
    for feat in feats:
        template.append("     (=> (<= FEAT_%s %%DELTA_FEAT_%s )" % (feat, feat))
        template.append("         (= DELTA_%s (- %%DELTA_FEAT_%s FEAT_%s )))" % (feat, feat, feat))
        template.append("     (=> (>  FEAT_%s %%DELTA_FEAT_%s )" % (feat, feat))
        template.append("         (= DELTA_%s (- FEAT_%s %%DELTA_FEAT_%s )))" % (feat, feat, feat))

    template.append("\n       ;; delta")
    template.append("         (= delta (+ DELTA_max_step_height_left")
    template.append("                     DELTA_min_step_height_left")
    template.append("                     DELTA_max_step_width_left")
    template.append("                     DELTA_min_step_width_left")
    template.append("                     DELTA_max_step_height_right")
    template.append("                     DELTA_min_step_height_right")
    template.append("                     DELTA_max_step_width_right")
    template.append("                     DELTA_min_step_width_right")
    template.append("                     DELTA_vertical_material")
    template.append("                     DELTA_horizontal_material))")

    # Compute separation

    template.append("\n       ;; separation")
    template.append("         (= separation (- cost delta))")

    # Efficiency constraints

    template.append("\n       ;; no left and right_ladder together")
    template.append("       (=> left_ladder (not right_ladder))")
    template.append("       (=> right_ladder (not left_ladder))")

#    template.append("\n       ;; max height cost is not smaller than dmax")
#    template.append("       (>= FEAT_max_step_height_left %d)" % (dmax))
#    template.append("       (>= FEAT_max_step_height_right %d)" % (dmax))
#
#    template.append("\n       ;; max height cost is not larger than dmax*n")
#    template.append("       (<= FEAT_max_step_height_left (* %d %d))" % (n, dmax))
#    template.append("       (<= FEAT_max_step_height_right (* %d %d))" % (n, dmax))
#
#    template.append("\n       ;; min height cost is not smaller than 0")
#    template.append("       (>= FEAT_min_step_height_left 0)")
#    template.append("       (>= FEAT_min_step_height_right 0)")
#
#    template.append("\n       ;; min height cost is not larger than dmax/n")
#    template.append("       (<= FEAT_min_step_height_left (/ %d %d))" % (dmax,n))
#    template.append("       (<= FEAT_min_step_height_right (/ %d %d))" % (dmax,n))
#
#    template.append("\n       ;; max width cost is not smaller than 0")
#    template.append("       (>= FEAT_max_step_width_left 0)")
#    template.append("       (>= FEAT_max_step_width_right 0)")
#
#    template.append("\n       ;; max width cost is not larger that dmax")
#    template.append("       (<= FEAT_max_step_width_left %d)" % (dmax))
#    template.append("       (<= FEAT_max_step_width_right %d)" % (dmax))
#
#    template.append("\n       ;; min width cost is not smaller than 0")
#    template.append("       (>= FEAT_min_step_width_left 0)")
#    template.append("       (>= FEAT_min_step_width_right 0)")
#
#    template.append("\n       ;; min width cost is not larger that dmax/n")
#    template.append("       (<= FEAT_min_step_width_left (/ %d %d))" % (dmax,n))
#    template.append("       (<= FEAT_min_step_width_right (/ %d %d))" % (dmax,n))

    template.append("\n       ;; material cost is not smaller than dmin")
    template.append("       (>= FEAT_vertical_material %d)" % (dmin))
    template.append("       (>= FEAT_horizontal_material %d)" % (dmin))

    template.append("\n       ;; material cost is not larger than dmax*n")
    template.append("       (<= FEAT_vertical_material %d)" % (dmax*n))
    template.append("       (<= FEAT_horizontal_material %d)" % (dmax*n))

    # Input/Output placeholders

    template.append("\n       %x ;; placeholder for x")

    template.append("\n       %y ;; placeholder for y")

    # Optimize

    template.append("   )")
    template.append(")")

    template.append("(minimize %cost )")
    template.append("(check-sat)")
    template.append("(set-model -1)")
    template.append("(get-model)")
    template.append("(exit)")

    return "\n".join(template)


def generate_pillar_horiz_left_data(n):
    points = [0]*(n+1)
    for i in range(1, n+1):
        points[i] = ["(= x_%d 0) " %i]
    for i in range(1, n):
        points[i].append("(= y_%d (/ %d %d)) " %(i,n-i,n))
    points[n].append("(= y_%d 0) " %n)
    for i in range(1, n+1):
        points[i].append("(= dx_%d (/ %d %d)) " %(i,i,n))
    for i in range(1, n+1):
        points[i].append("(= dy_%d (/ 1 %d)) " %(i,n))
    return points

def generate_pillar_horiz_right_data(n):
    points = [0]*(n+1)
    points[1] = ["(= x_1 0) "]
    for i in range(2,n+1):
        points[i] = ["(= x_%d (/ %d %d)) " %(i,i-1,n)]
    points[1].append("(= y_1 0) ")
    for i in range(2,n+1):
        points[i].append("(= y_%d (/ %d %d)) " %(i,i-1,n))
    for i in range(1,n+1):
        points[i].append("(= dx_%d (/ %d %d)) " %(i,n+1-i,n))
    for i in range(1,n+1):
        points[i].append("(= dy_%d (/ 1 %d)) " %(i,n))
    return points

def generate_pillar_vert_left_data(n):
    points=[0]*(n+1)
    for i in range(1,n+1):
        points[i]=["(= x_%d (/ %d %d)) " %(i,i-1,n)]
    for i in range(1,n+1):
        points[i].append("(= dx_%d (/ 1 %d)) " %(i,n))
    for i in range(1,n+1):
    	points[i].append("(= y_%d 0) "% i)
    for i in range(1,n+1):
        points[i].append("(= dy_%d (/ %d %d)) " %(i,n+1-i,n))
    return points

def generate_pillar_vert_right_data(n):
    points=[0]*(n+1)
    for i in range(1,n+1):
        points[i]=["(= x_%d (/ %d %d)) " %(i,i-1,n)]
    for i in range(1,n+1):
        points[i].append("(= dx_%d (/ 1 %d)) " %(i,n))
    for i in range(1,n+1):
    	points[i].append("(= y_%d 0) "% i)
    for i in range(1,n+1):
        points[i].append("(= dy_%d (/ %d %d)) " %(i,i,n))
    return points

def generate_floating_left_data(n):
    points=[0]*(n+1)
    points[1]=["(= x_1 0) "]
    for i in range(2,n+1):
        points[i]=["(= x_%d (/ %d %d)) " %(i,i-1,n)]
    for i in range(1,n):
        points[i].append("(= y_%d (/ %d %d)) " %(i,n-i,n))
    points[n].append("(= y_%d 0) " %n)
    for i in range(1,n+1):
        points[i].append("(= dx_%d (/ 1 %d)) " %(i,n))
    for i in range(1,n+1):
        points[i].append("(= dy_%d (/ 1 %d)) " %(i,n))
    return points

def generate_floating_right_data(n):
    points=[0]*(n+1)
    points[1]=["(= x_1 0) "]
    for i in range(2,n+1):
        points[i]=["(= x_%d (/ %d %d)) " %(i,i-1,n)]
    points[1].append("(= y_1 0) ")
    for i in range(2,n+1):
        points[i].append("(= y_%d (/ %d %d)) " %(i,i-1,n))
    for i in range(1,n+1):
        points[i].append("(= dx_%d (/ 1 %d)) " %(i,n))
    for i in range(1,n+1):
        points[i].append("(= dy_%d (/ 1 %d)) " %(i,n))
    return points

def to_rectangles(assignments, size):
    """Reads rows like::

        (= x_1 0.3)
        (= y_1 0.4)
        (= x_2 (/ 1 5))
        (= y_2 (/ 1 5))
        (= dx_1 (/ 1 5))
        (= dy_1 (/ 1 5))
        (= dx_2 (/ 1 5))
        (= dy_2 (/ 1 5))

    :param assignments: a list of assignments in the above format.
    :param size: size of the output picture.
    :returns: a list of rectangles.
    """
    data = defaultdict(dict)
    for assignment in assignments:
        variable, value = assignment.split()[1], libsmt2float(assignment) * size
        parts = variable.split("_")
        if parts[0] in ("x", "y", "dx", "dy"):
            name, index = parts[0], int(parts[1])
            data[name][index] = value
    num_blocks = len(data[data.keys()[0]])

    rectangles = []
    for i in range(1, num_blocks+1):
        x, y, dx, dy = data["x"][i], data["y"][i], data["dx"][i], data["dy"][i]
        rectangles.append([(x, y), (x+dx, y+dy)])
    return rectangles

def draw(path, x, y, pred_y, size=PNG_SIZE):
    COLORS_TRUE = {0: "red", 1: "green"}
    COLORS_PRED = {0: "red", 1: "green"}

#    true_rectangles = to_rectangles(x.assignments + y.assignments, size)
    # XXX pred_y.assignments includes *all* assignments, even for the variables
    # in x
    pred_rectangles = to_rectangles(pred_y.assignments, size)

    im = Image.new("RGB", (size, size), "white")
    draw = ImageDraw.Draw(im)

#    for i, rectangle in enumerate(true_rectangles):
#        draw.rectangle(rectangle, fill=COLORS_TRUE[i % 2])
#        print "true_rectangle[{}] = ({}, {}, {}, {})" \
#                .format(i, rectangle[0][0] / size,
#                           rectangle[0][1] / size,
#                           rectangle[1][0] / size,
#                           rectangle[1][1] / size)
    for i, rectangle in enumerate(pred_rectangles):
        draw.rectangle(rectangle, fill=COLORS_PRED[i % 2])
        print "pred_rectangle[{}] = ({}, {}, {}, {})" \
                .format(i, rectangle[0][0] / size,
                           rectangle[0][1] / size,
                           rectangle[1][0] / size,
                           rectangle[1][1] / size)

    ImageOps.flip(im).save(path, "PNG")

def main():

    MIN_TRAIN_BLOCKS, MAX_TRAIN_BLOCKS, MAX_TEST_BLOCKS = 2, 6, 8

    VARIANTS = (
        ("floating-right", generate_floating_right_data),
        ("pillars-horiz-right", generate_pillar_horiz_right_data),
        ("pillars-vert-right", generate_pillar_vert_right_data),
        ("floating-left", generate_floating_left_data),
        ("pillars-horiz-left", generate_pillar_horiz_left_data),
        ("pillars-vert-left", generate_pillar_vert_left_data),
    )

    # Generate the template files
    for num_blocks in range(MIN_TRAIN_BLOCKS, MAX_TEST_BLOCKS + 1):
        with open("template-{}.smt2".format(num_blocks), "wb") as fp:
            fp.write(generate_template(num_blocks))

    def blocks_to_assignments(blocks):
        return " , ".join(" , ".join(block) for block in blocks)

    # Generate the input files
    for name, generate in VARIANTS:

        print "processing '{}' stairways".format(name)

        # Generate all stairway configurations for the current stairway type
        blocks = [generate(num_blocks) for num_blocks
                  in range(MIN_TRAIN_BLOCKS, MAX_TEST_BLOCKS + 1)]

        # Write the input file
        for train_blocks in range(MIN_TRAIN_BLOCKS, MAX_TRAIN_BLOCKS + 1):

            print "processing stairway with {} training blocks".format(train_blocks)

            path = "input-{}-{}.lmt".format(name, train_blocks)

            # Write out all training and test examples
            with open(path, "wb") as fp:
                for n in range(MIN_TRAIN_BLOCKS, MAX_TEST_BLOCKS + 1):
                    blocks_for_n = blocks[n - MIN_TRAIN_BLOCKS]
                    x_str = blocks_to_assignments([blocks_for_n[1]])
                    y_str = blocks_to_assignments(blocks_for_n[2:])
                    mode = "train" if n <= train_blocks else "test"
                    fp.write("template-{}.smt2 ; {} ; {} ; {}\n".format(n, x_str, y_str, mode))

            # Run LMT proper on the input file we just generated
            lmt = LMTRunner("default", path, C, EPSILON, "optimathsat5",
                            debug=True)
            results = zip(lmt._test_xs, lmt._test_ys, lmt.run())

            # Draw the predictions
            for i, (x, y, pred_y) in enumerate(results):
                path = "prediction-{}-{}-@{}.png".format(name, train_blocks, train_blocks + i + 1)
                draw(path, x, y, pred_y)

if __name__ == "__main__":
    main()
